Changing the landscape of addiction recovery for professional men ages 30+. We treat the biology of addiction, healing your brain as your work on the biopsychosocial-spiritual aspects of recovery. 26-day residential experience with 11 months of supported aftercare for clients and their families.

Address: 648 N Bella Vista Dr, Orem, UT 84097, USA

Phone: 801-477-7493

Website: https://ampelisrecovery.com/
